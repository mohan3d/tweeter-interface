
# Tweeter-Interface  
A ReactJS application to consume [Tweeter API](https://bitbucket.org/mohan3d/tweeter/).

[Deployed version](https://tweeter-splitter.herokuapp.com) on [Heroku](https://heroku.com).


## Installation
```sh
$ cd tweeter-interface
$ npm install
```

## Run Locally
**Note:** Don't forget to run django project.

```sh
$ npm start
```

Your app should now be running on [localhost:3000](http://localhost:3000/).


## Testing 
```sh
$ npm run test
```

## Structure
```
src/tweeterapi.js: tweeter API wrapper in JS, makes it easy to interact with tweeter api.

src/splitter.js: logic code for splitting messages.

src/components: all components required for react app.
```