// Prefix length (x/y )
const PREFIX_LENGTH = 4;

// Maximum allowed length of message.
const MESSAGE_MAX_LENGTH = 50;

// Ratio between prefix length and message length.
// Used to calculate how many messages is needed,
// after appending indicators.
const PREFIX_TO_MESSAGE_RATIO = PREFIX_LENGTH / MESSAGE_MAX_LENGTH;

// Array of whitespaces.
const WHITE_SPACES = [' ', '\r', '\t', '\n'];

export { splitMessage };

/**
 * Creates new string with i/n appended before message.
 *
 * @param message   message to be prefixed.
 * @param i         message number
 * @param n         messages count
 * @returns {string}
 */
function addPrefixToMessage(message, i, n) {
  return `${i}/${n} ${message}`;
}

/**
 * Finds index of the last whitespace in s,
 * Returns -1 if no whitespace exists in s.
 *
 * @param s text to be searched
 * @returns {number}
 */
function lastIndexOfWhitespace(s) {
  for (let i = s.length - 1; i >= 0; i--) {
    if (WHITE_SPACES.indexOf(s[i]) !== -1) {
      return i;
    }
  }

  return -1;
}

/**
 * Splits text into multiple lines,
 * each line length doesn't exceed length.
 *
 * @param text      message to split.
 * @param length    maximum length of each part.
 * @returns {string[]}
 */
function split(text, length) {
  const messages = [];

  while (text.length > length) {
    const l = lastIndexOfWhitespace(text.substring(0, length));
    messages.push(text.substring(0, l));
    text = text.substring(l + 1);
  }

  messages.push(text);
  return messages;
}

/**
 * Checks if text has at least one span that has length greater than provided length,
 * which cannot be splitted.
 *
 * @param s         message to be checked
 * @param length    maximum allowed length
 * @returns {boolean}
 */
function hasSpansExceedingLength(s, length) {
  return s.split(/\s/)
    .map(x => x.length)
    .some(x => x > length);
}

/**
 * Splits message if it exceeds maximum limit (default 50 chars),
 * otherwise it will return an array containing only the message as it is.
 *
 * If message is splitted sub-messages will be prefixed with indicator.
 * An empty array will be returned in case message cannot be splitted.
 *
 * @param {string} text message to be splitted.
 * @returns {string[]}
 */
function splitMessage(text) {
  // Checks if text fits in one message.
  if (text.length <= MESSAGE_MAX_LENGTH) return [text];

  // Calculate required chunks count.
  const initialChunks = text.length / MESSAGE_MAX_LENGTH;
  const chunks = Math.ceil(initialChunks + initialChunks * PREFIX_TO_MESSAGE_RATIO);

  // Decrement message length by 2 more characters for each power of 10.
  // Decrement by 0 if chunks < 10   - Indicator x/y
  // Decrement by 2 if chunks < 100  - Indicator xx/yy
  // Decrement by 4 if chunks < 1000 - Indicator xxx/yyy
  const messageLength = MESSAGE_MAX_LENGTH - PREFIX_LENGTH - Math.trunc(Math.log10(chunks)) * 2;

  // Check if any span exceeded max allowed length.
  if (hasSpansExceedingLength(text, messageLength)) return [];

  // Actual split.
  const messages = split(text, messageLength);
  const messagesCount = messages.length;

  // Append indicator for each message.
  return messages.map((message, i) => addPrefixToMessage(message, i + 1, messagesCount));
}