import React, { Component } from 'react';
import Footer from './components/Footer';
import Header from './components/Header';
import TweetBox from './components/TweetBox'
import TweetList from './components/TweetList';

import { login, logout, isLoggedIn, getTweets, postTweet, deleteTweet } from './tweeterapi';
import { splitMessage } from './splitter';

import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      tweets: [],
      userLoggedIn: isLoggedIn(),
      posting: false,
    };

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.postTweet = this.postTweet.bind(this);
    this.getTweets = this.getTweets.bind(this);
    this.deleteTweet = this.deleteTweet.bind(this);
  }

  resetState() {
    this.setState({
      tweets: [],
      userLoggedIn: isLoggedIn(),
      posting: false,
    });
  }

  handleError(err) {
    // TODO: handle errors in UI level.
    console.log(err);
  }

  login(username, password) {
    login(username, password)
    .then(resp => {
      this.setState({ userLoggedIn: true });
      this.getTweets();
      return resp;
    })
    .catch(err => this.handleError(err));
  }

  logout() {
    logout();
    this.resetState();
  }

  async postTweet(content){
    this.setState({ posting: true });
    
    const messages = splitMessage(content);
    for(let message of messages){
      try {
        let resp = await postTweet(message).catch(err => this.handleError(err));
        let tweet = resp.data;

        this.setState((prevState) => ({
          tweets: [tweet].concat(prevState.tweets)
        }));
      } catch(err) { 
        this.handleError(err);
      }      
    }

    this.setState({ posting: false });
  }

  getTweets() {
    getTweets()
    .then(resp => resp.data)
    .then(tweets => this.setState({ tweets }))
    .catch(err => this.handleError(err));
  }

  deleteTweet(tweetID) {
    deleteTweet(tweetID)
    .then(resp => {
      this.setState((prevState) => ({
        tweets: prevState.tweets.filter(tweet => tweet.id !== tweetID)
      }));
    })
    .catch(err => this.handleError(err));
  }

  componentDidMount() {
    if(this.state.userLoggedIn) {
      this.getTweets();
    }
  }

  render() {
    return (
      <div className="app">
        <Header userLoggedIn={this.state.userLoggedIn} login={this.login} logout={this.logout} />
        {
          this.state.userLoggedIn &&
          <TweetBox posting={this.state.posting} onClick={this.postTweet} />          
        }
        {
          this.state.userLoggedIn &&
          <TweetList tweets={this.state.tweets} onClick={this.deleteTweet} />
        }
        {
          !this.state.userLoggedIn && 
          <div className="description">
            <h1>Tweeter helps you share long tweets</h1>
            <h2>Post what’s happening to you right now</h2>
          </div>
        }
        <Footer />
      </div>
    );
  }
}

export default App;