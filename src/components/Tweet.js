import React, { Component } from 'react';


class Tweet extends Component {
  render() {
    return (
      <div className="tweet">
        <div className="content">{this.props.tweet.content}</div>
        <div className="controls">
          <button onClick={() => this.props.onClick(this.props.tweet.id)}>
            Delete
          </button>
        </div>
      </div>
    )
  }
}

export default Tweet;