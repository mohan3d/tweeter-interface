import React, { Component } from 'react';
import Tweet from './Tweet';


class TweetList extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <h1>Your tweets</h1>
          <div className="tweets">
          {this.props.tweets.map(tweet => <Tweet onClick={this.props.onClick} key={tweet.id} tweet={tweet} />)} 
          </div>  
        </div>   
      </section>  
    );
  }
}

export default TweetList;