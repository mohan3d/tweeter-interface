import React, { Component } from 'react';
import logo from '../logo.svg';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  
  handleUsernameChange(event) {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleLogin(event) {
    event.preventDefault();
    if(!(this.state.username && this.state.password)) return;

    this.props.login(this.state.username, this.state.password);
  }

  handleLogout(event) {
    event.preventDefault();

    this.setState({ username: '', password: '' });
    this.props.logout();
  }

  loginForm() {
    return (
      <form>
        <div className="form-input">
          <label>
            Username
            <input onChange={this.handleUsernameChange} value={this.state.username} type="text" name="username" />
          </label>
        </div>
        <div className="form-input">
          <label>
            Password
            <input onChange={this.handlePasswordChange} value={this.state.password} type="password" name="password" />
          </label>
        </div>
        <button onClick={this.handleLogin}>Login</button>
      </form>
    );
  }

  logoutForm() {
    return (
      <form>
        <button onClick={this.handleLogout}>Logout</button>
      </form>
    );
  }

  render() {
    const form = this.props.userLoggedIn ? this.logoutForm() : this.loginForm();

    return (
      <header>
        <div className="topbar">
          <div>
            <div className="logo">
              <img className="tweeter-logo" alt="tweeter-logo" src={logo} />            
            </div>          
            <div className="title">Tweeter</div>
          </div>
          <div>
            {form}
          </div>
        </div>        
      </header>
    );
  }
}

export default Header;