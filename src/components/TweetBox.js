import React, { Component } from 'react';


class TweetBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ content: event.target.value });
  }

  handleClick() {
    if(!this.state.content) return;
    
    this.props.onClick(this.state.content);
    this.setState({ content: '' });
  }

  render() {
    return (
      <section className="tweetbox">
        <div className="container">

          <div>
            <h1>Tweeter chunks your messages</h1>
            <h2>Post your long tweets, we will chunk it!</h2>
          </div>

          <div className="tweetbox">
            <div className="tweetcontent">
                <textarea
                onChange={this.handleChange} 
                value={this.state.content} 
                placeholder="What's happening?">
                </textarea>
            </div>

            <div className="controls">
              <button
              disabled={this.props.posting}
              onClick={() => this.handleClick()}>
                Tweet
              </button>
            </div>            
          </div>

        </div>        
      </section>
    );
  }
}

export default TweetBox;