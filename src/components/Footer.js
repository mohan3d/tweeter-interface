import React, { Component } from 'react';


class Footer extends Component {
  render() {
    return (
      <footer>
          <div>
            <a href="https://bitbucket.org/mohan3d/tweeter-interface">Bitbucket</a>
          </div>
          <div>&copy; Tweeter 2018</div>
          <div><a href="https://global-fashion-group.com/">GFG TASK</a></div>
      </footer>       
    );
  }
}

export default Footer;