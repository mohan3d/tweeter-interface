import axios from 'axios';

const TWEETER_AUTH_TOKEN = 'tweeterauthtoken';

const API_VERSION = 'v1';
const API_URL = `http://localhost:8000/api/${API_VERSION}`;

export {
  login,
  logout,
  isLoggedIn,
  getTweets,
  postTweet,
  deleteTweet
};

/**
 * Reads authorization token from localStorage.
 *
 * @returns {string}
 */
function getAuthorizationToken() {
  return localStorage.getItem(TWEETER_AUTH_TOKEN);
}

/**
 * Returns true if there is no saved authorization token in localStorage.
 *
 * @returns {boolean}
 */
function isLoggedIn() {
  return getAuthorizationToken() !== null;
}

/**
 * Creates headers object with optional Authorization header.
 * If no authToken exists Authorization header is removed.
 *
 * @param authToken authorization token.
 * @returns {object}
 */
function getHeaders(authToken) {
  const token = authToken !== null ? authToken : getAuthorizationToken();
  const headers = {};

  if (token != null) {
    headers['Authorization'] = ` Token ${token}`;
  }

  return headers;
}

/**
 * Login with provided credentials,
 * returns promise with authToken from server.
 *
 * If loggedIn successfully authToken is saved to localStorage.
 *
 * @param username credentials username.
 * @param password credentials password.
 * @returns {Promise<string>}
 */
function login(username, password) {
  return axios.post(`${API_URL}/login/`, {
      username,
      password
    })
    .then(resp => {
      const token = resp.data.token;
      localStorage.setItem(TWEETER_AUTH_TOKEN, token);
      return resp;
    });
}

/**
 * Removes AuthToken from localStorage.
 */
function logout() {
  localStorage.removeItem(TWEETER_AUTH_TOKEN);
}

/**
 * Requests tweets list of logged-in user.
 *
 * @returns {Promise<Response>}
 */
function getTweets() {
  return axios.get(`${API_URL}/tweets/`, {
    headers: getHeaders(null)
  });
}

/**
 * Creates new tweet with the provided content.
 *
 * @param content text of tweet to be created.
 * @returns {Promise<Response>}
 */
function postTweet(content) {
  return axios.post(`${API_URL}/tweets/`, { content }, {
    headers: getHeaders(null)
  });
}

/**
 * Deletes existing tweet with the provided tweetID.
 *
 * @param tweetID tweet id to be deleted.
 * @returns {Promise<Response>}
 */
function deleteTweet(tweetID) {
  return axios.delete(`${API_URL}/tweets/${tweetID}/`, {
    headers: getHeaders(null)
  });
}