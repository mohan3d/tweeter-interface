import { splitMessage } from './splitter';

describe('splitMessage', () => {
  it('should output one message with no indicators', () => {
    const message = "This message has less than 50 chars";
    expect(splitMessage(message)).toEqual([
      "This message has less than 50 chars"
    ]);
  });

  it('should output one message with no indicators', () => {
    const message = "This message should be displayed with no indicator";
    expect(splitMessage(message)).toEqual([
      "This message should be displayed with no indicator"
    ]);
  });

  it('shoud output two messages with indicators', () => {
    const message = "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself.";
    expect(splitMessage(message)).toEqual([
      "1/2 I can't believe Tweeter now supports chunking",
      "2/2 my messages, so I don't have to do it myself."
    ]);
  });

  it('should output 3 messages each has less than or equal 50 chars', () => {
    // message = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa " * 5
    const message = ('a'.repeat(45) + ' ').repeat(3);

    expect(splitMessage(message)).toEqual([
      "1/3 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "2/3 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "3/3 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ",
    ]);
  });

  it('should output empty array', () => {
    const message = 'a'.repeat(51);
    expect(splitMessage(message)).toEqual([]);
  });

  it('should output empty array', () => {
    const message = 'A message with ' + 'b'.repeat(50) + ' in the middle of it';
    expect(splitMessage(message)).toEqual([]);
  });
});